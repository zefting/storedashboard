import { Component, OnInit } from '@angular/core';
// ng charts
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Color, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import {HttpService} from './../http.service'

//
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  recentOrders = [];
  LeadingProduct = [];
  currentChampaings =[];

  constructor(private _httpService: HttpService) { }
  // handle bar chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartPersonelSalesLabels: Label[] = [];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barPersonelSalesChartData: ChartDataSets[] = [];
  // handle Linechars

  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [

    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

   // events
   public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
   public lineChartData: ChartDataSets[] = [
    { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];
  /*
  public growth()
  {
    const chartImport = this._httpService.growthMonthOverMonth().subscribe(data =>data);
    for(let data of chartImport)
    {
      Object.keys(data)
    }

  }*/
  ngOnInit(): void {
    /*
    this._httpService.getAllOrders().subscribe(data =>
      data = this.recentOrders
    );

    this._httpService.leadingSales().subscribe(data =>data = this.LeadingProduct)
*/
    this.recentOrders = [
      {"id":"1", "costumer":"Kal El", "shipped":0},
      {"id":"2", "costumer":"Diana Prince", "shipped":0},
      {"id":"3", "costumer":"Jor El", "shipped":0},
      {"id":"4", "costumer":"Clark Kent", "shipped":1},
      {"id":"5", "costumer":"Louis Lane", "shipped":1},
      {"id":"6", "costumer":"Bruce Wane", "shipped":1}
    ]
    this.LeadingProduct = [
      {"productname":"Red Shirt", "type":"Red", "size":"Large", "sold":10},
      {"productname":"T-shirt", "type":"Blue", "size":"Small", "sold":20},
      {"productname":"Shoe", "type":"Yellow", "size":"42", "sold":30},
      {"productname":"Purse", "type":"Red", "size":"One-size", "sold":5},
      {"productname":"Bag", "type":"Backpack", "size":"35", "sold":11}
    ]


   //mock json som api
    this.currentChampaings =
    [
      {"champagin id":1, "name":"Ipsum", "timeRemaning" : 10},
      {"champagin id":1, "name":"Lorem", "timeRemaning" : 90},
      {"champagin id":1, "name":"Dolor", "timeRemaning" : 70}
    ];

    this.barChartPersonelSalesLabels = ['John', 'Ip', 'Lorel', 'Julie', 'Cesear', 'Constantine'];
    this.barPersonelSalesChartData = [
      { data: [65, 59, 80, 81, 56, 55], label: 'This Month' },
      { data: [28, 48, 40, 19, 86, 27], label: 'Last Month' }
    ]
  }

}
