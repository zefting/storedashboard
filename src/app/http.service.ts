import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }
  basrOrderUrl = "https://localhost:5001/api/orders/"

  getAllOrders(){
    return this.http.get(this.basrOrderUrl);
  }

  getOrderById(orderId) {
    return this.http.get(this.basrOrderUrl + orderId);
  }
  getSalesByEmployee()
  {
    return this.http.get(this.basrOrderUrl + "salesbyemployee");
  }
  leadingSales()
  {
    return this.http.get(this.basrOrderUrl + "leadingsales");
  }

  timeRemaning()
  {
    return this.http.get(this.basrOrderUrl + "timeremaning");
  }

  growthMonthOverMonth()
  {
    return this.http.get(this.basrOrderUrl + "growthmonthovermonth");
  }

}
