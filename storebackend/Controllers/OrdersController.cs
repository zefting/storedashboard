using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using storebackend.Data;
using storebackend.Models;
using AutoMapper;
using storebackend.Dtos;


namespace storebackend.Controllers
{
  [Route("api/orders")]
  [ApiController]
  public class OrdersController : ControllerBase
  {
    private readonly IStoreRepo _repository;
    private readonly IMapper _mapper;

    public OrdersController(IStoreRepo repository, IMapper mapper)
    {

      _repository = repository;
      _mapper = mapper;

    }
     public ActionResult<IEnumerable<OrdersDto>> GetAllOrders()
    {
      var orderItems = _repository.GetAllOrders();
      return Ok(_mapper.Map<IEnumerable<OrdersDto>>(orderItems));
    }
    [Route("{id}")]
    public ActionResult<OrdersDto> GetOrderById(int orderId)
    {
      var orderItem = _repository.GetOrderById(orderId);
      return null;
    }
    [Route("salesbyemployee")]
    public ActionResult<SalesCountDto> GetSalesByEmployee()
    {
      var orderItems = _repository.GetAllOrders();
      var employees = _repository.GetAllEmployees();
      var salesCounts = new List<SalesCountDto>();
      foreach(var employee in employees)
      {
         // new instance of SalesCountDto
          var salesCount = new SalesCountDto();
          // populate the salescount object with the employee name and the number of sold items
          // Get the same of the employee
          salesCount.Name = _repository.GetEmployeeById(employee.EmployeeId).Name;
          // count the number of times an employee made a sale
          salesCount.employeeCount = (from orders in orderItems where orders.SoldBy == employee.EmployeeId select orders.SoldBy).Count();
          // add the object to the list
          salesCounts.Add(salesCount);
      }
      return Ok(salesCounts);
    }

    [Route("leadingsales")]
    public ActionResult<List<LeadingProductsDto>> LeadingSales()
    {
      var orderItems = _repository.GetAllOrders();
      var orderProductsItems = _repository.GetAllOrdersProducts();
      DateTime currentdate =  DateTime.Now;
      var orderwithproducts = new List<OrderProducts>();
      orderItems = orderItems.Where(order => order.SoldDate <= currentdate && order.SoldDate > currentdate.AddDays(-14)).ToList();
      var productcounts = new SortedDictionary<int, int>();
      var leadingProductsDtos = new List<LeadingProductsDto>();
      foreach(var order in orderItems)
      {
        foreach(var orderProduct in orderProductsItems)
        {
          if(order.OrderId == orderProduct.OrderId)
          {
            if(productcounts.Keys.Contains(orderProduct.ProductId))
            {
              productcounts[orderProduct.ProductId] += 1;
            }
            else
            {
              productcounts.Add(orderProduct.ProductId, 1);
            }
          }
        }
      }
      productcounts.OrderByDescending(product => product.Value);
      var firstFiveProducts = productcounts.Take(5).ToDictionary(key => key.Key, value => value.Value);
      foreach(var product in firstFiveProducts)
      {
        var productFromDB = _repository.GetProductById(product.Key);
        var leadingProduct = new LeadingProductsDto();
        leadingProduct.productId = productFromDB.Productid;
        leadingProduct.productname = productFromDB.Productname;
        leadingProduct.size = productFromDB.Size;
        leadingProduct.type = productFromDB.Type;
        leadingProduct.sold = product.Value;
        leadingProductsDtos.Add(leadingProduct);
      }
      return Ok(leadingProductsDtos);
    }

    [Route("timeremaning")]
    public ActionResult<IEnumerable<TimeRemaningDto>> Timeremaning()
    {
      var champaginItems = _repository.GetAllChampagins();
      champaginItems.Where(champagin => champagin.Active == 1).ToList();
      var timeRemaningDtos = new List<TimeRemaningDto>();
      foreach(var champagin in champaginItems)
      {
        if(champagin.EndDate.HasValue)
        {
          var timeRemaningDto = new TimeRemaningDto();
          timeRemaningDto.TimeRemaning = (int) (champagin.EndDate.Value.Subtract(champagin.StartDate)).TotalDays;
          timeRemaningDto.Name = champagin.Name;
          timeRemaningDto.ChampaginId = champagin.ChampaginId;
          timeRemaningDtos.Add(timeRemaningDto);
        }

      }
      return Ok(timeRemaningDtos);
    }
    [Route("growthmonthovermonth")]
    public ActionResult<IEnumerable<SalesByMonthDto>> GrowthMounthOverMonth()
    {
      // Percent increase (or decrease) = (Period 2 – Period 1) / Period 1 * 100
      // https://clevertap.com/blog/month-over-month/
      var orderItems = _repository.GetAllOrders();
      var mounthOverMonthGrowthDto = new MounthOverMonthGrowthDto();
      DateTime today = DateTime.Now;
      var salesByMonthDtos = new List<SalesByMonthDto>();

      DateTime oneYear = today.AddMonths(-12);
      while( oneYear <= today )
      {

        var salesByMonthDto = new SalesByMonthDto();
        salesByMonthDto.salesByMonth = (from order in orderItems where order.SoldDate.Month == oneYear.Month select order.OrderId).Count();

        salesByMonthDto.month = oneYear.ToString("MMMM");
        salesByMonthDtos.Add(salesByMonthDto);
        oneYear = oneYear.AddMonths(1);
      }
      var growthByMonth = new Dictionary<string, int>();
      int counter = 0;
      while(counter <= 11)
      {
        var procentpermonth = (salesByMonthDtos[counter + 1].salesByMonth - salesByMonthDtos[counter].salesByMonth) / salesByMonthDtos[counter].salesByMonth * 100;
        // Percent increase (or decrease) = (Period 2 – Period 1) / Period 1 * 100
        growthByMonth.Add(salesByMonthDtos[counter].month, procentpermonth);
        ++counter;
      }
     return Ok(growthByMonth);
    }
    [Route("salesbyemploylasttwomonths")]
    public ActionResult<IEnumerable<SalesByEmployDto>> SalesByEmployLastTwoMonths()
    {
      var orders = _repository.GetAllOrders();
      var employ = _repository.GetAllEmployees();
      var currentMonth = DateTime.Now;
      var SalesByEmployDtos = new List<SalesByEmployDto>();
      // orderItems = orderItems.Where(order => order.SoldDate <= currentdate && order.SoldDate > currentdate.AddDays(-14)).ToList();
      var ordersThisMonth = new Dictionary<int, string>();
      ordersThisMonth.Add(orders.Where(order => order.SoldDate.Month <= currentMonth.Month && currentMonth.Month < order.SoldDate.Month).Count(), "This Month");

      var ordersLastMonth = new Dictionary<int, string>();
      ordersLastMonth.Add(orders.Where(order => order.SoldDate.Month <= currentMonth.Month && order.SoldDate.Month > currentMonth.AddMonths(-1).Month).Count(), "Last Month");
      foreach(var employee in employ)
      {
        var SalesByEmployDto = new SalesByEmployDto();
        var dict = new Dictionary<object,object>();
        SalesByEmployDto.label = (from sale in orders where sale.SoldBy == employee.EmployeeId select employee.Name).FirstOrDefault();
        dict.Add(ordersLastMonth, ordersLastMonth);
        SalesByEmployDto.data = dict;
        SalesByEmployDtos.Add(SalesByEmployDto);
      }
      return SalesByEmployDtos;
    }
  }
}
