﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace storebackend.Models
{
    public partial class dashboardContext : DbContext
    {
        public dashboardContext()
        {
        }

        public dashboardContext(DbContextOptions<dashboardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Champagin> Champagin { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<OrderProducts> OrderProducts { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Products> Products { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Champagin>(entity =>
            {
                entity.ToTable("champagin");

                entity.Property(e => e.ChampaginId)
                    .HasColumnName("champaginId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.EndDate).HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnName("startDate");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PRIMARY");

                entity.ToTable("employees");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("employeeId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderProducts>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("orderProducts");

                entity.HasIndex(e => e.OrderId)
                    .HasName("orderproduct");

                entity.HasIndex(e => e.ProductId)
                    .HasName("productorder");

                entity.Property(e => e.OrderId)
                    .HasColumnName("orderId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Order)
                    .WithMany()
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orderproduct");

                entity.HasOne(d => d.Product)
                    .WithMany()
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("productorder");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PRIMARY");

                entity.ToTable("orders");

                entity.HasIndex(e => e.SoldBy)
                    .HasName("soldBy_id");

                entity.Property(e => e.OrderId)
                    .HasColumnName("orderId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ChampaginId)
                    .HasColumnName("champaginId")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Costumer)
                    .IsRequired()
                    .HasColumnName("costumer")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Shipped)
                    .HasColumnName("shipped")
                    .HasColumnType("tinyint(4)");

                entity.Property(e => e.SoldBy)
                    .HasColumnName("soldBy")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SoldDate)
                    .HasColumnName("soldDate")
                    .HasDefaultValueSql("'current_timestamp()'");

                entity.HasOne(d => d.SoldByNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.SoldBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("soldBy_id");
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.HasKey(e => e.Productid)
                    .HasName("PRIMARY");

                entity.ToTable("products");

                entity.Property(e => e.Productid)
                    .HasColumnName("productid")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Productname)
                    .IsRequired()
                    .HasColumnName("productname")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(110)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
