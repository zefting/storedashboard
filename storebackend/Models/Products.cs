﻿using System;
using System.Collections.Generic;

namespace storebackend.Models
{
    public partial class Products
    {
        public int Productid { get; set; }
        public string Productname { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
    }
}
