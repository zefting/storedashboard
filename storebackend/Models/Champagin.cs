﻿using System;
using System.Collections.Generic;

namespace storebackend.Models
{
    public partial class Champagin
    {
        public int ChampaginId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte Active { get; set; }
    }
}
