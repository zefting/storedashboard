﻿using System;
using System.Collections.Generic;

namespace storebackend.Models
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public string Costumer { get; set; }
        public byte Shipped { get; set; }
        public DateTimeOffset SoldDate { get; set; }
        public int SoldBy { get; set; }
        public int? ChampaginId { get; set; }

        public virtual Employees SoldByNavigation { get; set; }
    }
}
