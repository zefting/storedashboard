using storebackend.Dtos;
using storebackend.Models;
using AutoMapper;

namespace storebackend.Profiles
{
    public class ProductsProfile : Profile
    {
      public ProductsProfile()
      {
        CreateMap<Products, ProductsDtos>();
        CreateMap<ProductsDtos, Products>();
      }
    }
}
