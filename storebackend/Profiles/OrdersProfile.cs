using storebackend.Dtos;
using storebackend.Models;
using AutoMapper;

namespace storebackend.Profiles
{
    public class OrdersProfile : Profile
    {
      public OrdersProfile()
      {
        CreateMap<Orders, OrdersDto>();
        CreateMap<OrdersDto, Orders>();
      }
    }
}
