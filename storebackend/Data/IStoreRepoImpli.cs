using storebackend.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace storebackend.Data
{
  public class storebackendRepoImpli : IStoreRepo
  {
    private readonly dashboardContext _context;
    public storebackendRepoImpli(dashboardContext context)
    {
      _context = context;
    }
    public IEnumerable<Orders> GetAllOrders()
    {
      return _context.Orders.ToList();
    }
    public IEnumerable<OrderProducts> GetAllOrdersProducts()
      {
            return _context.OrderProducts.ToList();
      }

    public List<Products> GetProductsByOrder(int orderId)
        {
            var orderProductsItems = _context.OrderProducts.ToList();
            var products = new List<Products>();

                foreach(var orderProduct in orderProductsItems)
                {
                     if(orderProduct.OrderId == orderId)
                     {
                        products.Add(orderProduct.Product);
                     }
                }

            return products;
        }
    public Orders GetOrderById(int OrderId)
    {
      return _context.Orders.FirstOrDefault(o => o.OrderId == OrderId);
    }

    public IEnumerable<Champagin> GetAllChampagins()
    {
      return _context.Champagin.ToList();
    }
    public Champagin GetChampaginById(int champaginId)
    {
      return _context.Champagin.FirstOrDefault(c => c.ChampaginId == champaginId);
    }
    public IEnumerable<Orders> GetSalesByEmployee()
    {
     return _context.Orders.ToList();
    }
    public IEnumerable<Employees> GetAllEmployees()
    {
      return _context.Employees.ToList();
    }
    public Employees GetEmployeeById (int emplyId)
    {
      return _context.Employees.FirstOrDefault(e => e.EmployeeId == emplyId);
    }
    public IEnumerable<Products> GetAllProducts()
    {
      return _context.Products.ToList();
    }
    public Products GetProductById(int ProductId)
    {
      return _context.Products.FirstOrDefault(p => p.Productid == ProductId);
    }
    public bool SaveChanges()
    {
      return(_context.SaveChanges() >=0);
    }
  //  IStoreRepo.SaveChanges()
    // List<Orders> GetProductsByOrder
  }
}
