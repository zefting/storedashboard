using storebackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace storebackend.Data
{
  public interface IStoreRepo
  {
    bool SaveChanges();
    IEnumerable<Orders> GetAllOrders();
    Orders GetOrderById (int orderId);
    List<Products> GetProductsByOrder (int orderId);

    IEnumerable<Orders> GetSalesByEmployee();
    IEnumerable<Employees> GetAllEmployees();
    Employees GetEmployeeById (int emplyId);
    IEnumerable<OrderProducts> GetAllOrdersProducts();
    IEnumerable<Champagin> GetAllChampagins();
    Champagin GetChampaginById(int champaginId);

   //products
    IEnumerable<Products> GetAllProducts();
    Products GetProductById(int ProductId);
  }
}
