﻿using System;
using System.Collections.Generic;
using storebackend.Models;
namespace storebackend.Dtos
{
    public partial class EmployeesDtos
    {

        public int EmployeeId { get; set; }
        public string Name { get; set; }

    }
}
