﻿using System;
using System.Collections.Generic;

namespace storebackend.Dtos
{
    public partial class ChampaginDto
    {
        public int ChampaginId { get; set; }
        public string Name { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
