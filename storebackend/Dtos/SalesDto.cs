  using System;
  using System.Collections;
  using System.Collections.Generic;
  namespace storebackend.Dtos
  {
    public class SalesDataDto
    {
      public string label {get; set;}
      public List<int> data {get; set;}
    }
  }
