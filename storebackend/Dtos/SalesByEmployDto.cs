  using System;
  using System.Collections;
  using System.Collections.Generic;
  namespace storebackend.Dtos
  {
    public class SalesByEmployDto
    {
      public string label {get; set;}
      public Dictionary<object,object> data {get; set;}
    }
  }
