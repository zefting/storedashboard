using System;
using System.Collections.Generic;

namespace storebackend.Dtos
{
    public partial class TimeRemaningDto
    {
        public int ChampaginId { get; set; }
        public string Name { get; set; }
        public int TimeRemaning {get; set;}
    }
}
