﻿using System;
using System.Collections.Generic;
using storebackend.Models;
namespace storebackend.Dtos
{
    public partial class OrderProductsDto
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }

        public virtual Orders Order { get; set; }
        public virtual Products Product { get; set; }
    }
}
