﻿using System;
using System.Collections.Generic;
using storebackend.Models;
namespace storebackend.Dtos
{
    public partial class OrdersDto
    {
        public int OrderId { get; set; }
        public string Costumer { get; set; }
        public byte Shipped { get; set; }
        public DateTimeOffset Date { get; set; }
        public int SoldBy { get; set; }

        public  List<OrderProductsDto> order {get; set;}

        public virtual Employees SoldByNavigation { get; set; }
    }
}
