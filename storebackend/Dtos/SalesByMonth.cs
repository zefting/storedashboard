  namespace storebackend.Dtos
  {
    public class SalesByMonthDto
    {
      public string month {get; set;}
      public int salesByMonth {get; set;}
    }
  }

