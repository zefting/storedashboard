﻿using System;
using System.Collections.Generic;

namespace storebackend.Dtos
{
    public partial class ProductsDtos
    {
        public int Productid { get; set; }
        public int Productname { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }

    }
}
