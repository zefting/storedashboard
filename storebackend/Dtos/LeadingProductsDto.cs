

namespace storebackend.Dtos
{
  public class LeadingProductsDto
  {
    public int productId {get; set;}
    public string productname {get; set;}
    public string type {get; set;}
    public string size {get; set;}
    public int sold {get; set;}
  }
}
